import React from 'react';
import './App.css';
import Drawings from './components/DrawingList/DrawingList';

function App() {
  return (
    <div className="App">
      <h1>Drawings:</h1>
      <Drawings />
      <p>It is working!</p>
    </div>
  );
}

export default App;
