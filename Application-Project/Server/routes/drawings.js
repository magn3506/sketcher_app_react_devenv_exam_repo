// Create a new router object to handle requests.
const router = require('express').Router();

// MONGODB connection
const client = require('../connection');

// Get request drawings
router.get('/drawings', (req, res) => {
  // Connect to database
  client.connect((error) => {
    if (!error) {
      // Create a variable of the collection
      const collection = client.db('sketcherDB').collection('drawings');

      // Find all drawings and return an array that contains all the documents
      collection.find().toArray((queryError, result) => {
        if (queryError) throw queryError;
        console.log(result);
        return res.status(200).send(result);
      });
    } else {
      return res.sendStatus(404);
    }
    client.close();
  });
});

// Dummy route for testing the subcutaneous testing
router.get('/users', (req, res) => res.sendStatus(200));

// Export the router for the app
module.exports = router;
