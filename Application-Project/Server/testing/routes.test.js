const request = require('supertest');
const app = require('../app');

describe('Test of the different paths', () => {
  test('Users endpoind should respond with status 200', () => {
    request(app)
      .get('/users')
      .then((response) => {
        expect(response.statusCode).toBe(200);
        return response;
      });
  });

  test('Drawings should be returned in the JSON format', () => {
    request(app)
      .get('/drawings')
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.headers.contentType).toMatch(/json/);
        return response;
      });
  });
});
