// Require express
const express = require('express');
// Create a express application called app
const app = express();
const path = require('path');
// ENV
require('dotenv').config();

// Use json and urlencoded test
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Search static files
app.use(express.static(path.join(path.resolve(__dirname, '..'), 'client/build')));

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PATCH, POST, GET, DELETE');
  next();
});

// ROUTESS
const drawingsRoute = require('./routes/drawings');
// Tell express to use route
app.use(drawingsRoute);

app.get('*', (req, res) => {
  res.sendFile(path.join(path.resolve(__dirname, '..'), '/client/build/index.html'));
});

module.exports = app;
