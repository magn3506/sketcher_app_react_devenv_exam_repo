const app = require('./app');
// ENV
require('dotenv').config();

const PORT = process.env.PORT || 8080;

app.listen(PORT, (error) => {
  if (error) {
    console.log(`There is an error running on the server ${error}`);
  }
  console.log(`Server is running on port ${PORT}`);
});
