## Sketcher Application for Node.js/React & Development Environments

This is a repository for Node.js/React & Development Environments electives at Web Development INT KEA. 
The project is created and maintained by Anastazja Galuza, Magnus Vagn Jensen & Jakob Falkenberg Hansen.


## Documentation
https://magn3506.gitlab.io/sketcher_app_react_devenv_exam_repo/

## Sketcher app QA site
https://sketcher-app-qa.herokuapp.com/ 

## Sketcher app STAGING site
https://sketcher-app-staging.herokuapp.com/

## Sketcher app PRODUCTION site
https://sketcher-app.herokuapp.com/

